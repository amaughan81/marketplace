Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: :accounts }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'pages#index'
  post 'pages', to: 'pages#create'
  resources :checkout

  scope 'my-account' do
    resources :cards
    get 'profile', to: 'accounts#edit'
  end
  namespace :admin do
    resources :users
  end
end
