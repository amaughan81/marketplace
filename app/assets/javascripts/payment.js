var card;
$(function() {
    handlePayment();
    googlePay();
    $(document).on('change', '#card_select', function() { toggleCardPayment($(this)) })
});

function toggleCardPayment(opt) {
    if(opt.val() != 0) {
        $('#card_name').prop('disabled', true);
        $('#card-element').html('');
        $('#new_card_payment').hide();
        $('#payment-form').off('submit');
        //card.unmount('#card-element')
    } else {
        $('#new_card_payment').show();
        $('#card_name').prop('disabled', false);
        handlePayment();
    }
}

function handlePayment() {
    if($('#payment-form').length == 0) {
        return ;
    }

    var stripe = Stripe($('#stripe_pub_key').val());

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            lineHeight: '18px',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    card = elements.create('card', {style: style, hidePostalCode: true});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
        if($('#card_select').val() != 0) {
            return ;
        }

        event.preventDefault();

        var name = $("#card_name");
        var additionalData = {
            currency: 'gbp',
            name: name ? name.val() : undefined
        }

        stripe.createToken(card, additionalData).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });
}

function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
}

function googlePay() {
    if($('#stripe_pub_key').length == 0) {
        return ;
    }
    var stripe = Stripe($('#stripe_pub_key').val());
    var paymentRequest = stripe.paymentRequest({
        country: 'GB',
        currency: 'gbp',
        total: {
            label: 'Demo total',
            amount: 30,
        },
        requestPayerName: true,
        requestPayerEmail: true,
    });

    var elements = stripe.elements();
    var prButton = elements.create('paymentRequestButton', {
        paymentRequest: paymentRequest,
    });

// Check the availability of the Payment Request API first.
    paymentRequest.canMakePayment().then(function(result) {
        if (result) {
            prButton.mount('#payment-request-button');
        } else {
            document.getElementById('payment-request-button').style.display = 'none';
        }
    });

    paymentRequest.on('token', function(ev) {
        console.log($('meta[name="csrf-token"]').attr('content'))
        // Send the token to your server to charge it!
        fetch('/checkout', {
            method: 'POST',
            body: JSON.stringify({stripeToken: ev.token.id}),
            headers: {
                'content-type': 'application/json',
                'X-CSRF-Token': Rails.csrfToken()
            },
            credentials: 'same-origin'
        })
            .then(function(response) {
                if (response.ok) {
                    // Report to the browser that the payment was successful, prompting
                    // it to close the browser payment interface.
                    ev.complete('success');
                } else {
                    // Report to the browser that the payment failed, prompting it to
                    // re-show the payment interface, or show an error message and close
                    // the payment interface.
                    ev.complete('fail');
                }
            });
    });
}