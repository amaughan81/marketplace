class CheckoutController < ApplicationController
  before_action :get_customer, only: :create

  def index
    if current_user.customer_id.present?
      @cards = Stripe::Customer.retrieve(current_user.customer_id).sources.all(object: 'card')
    else
      @cards = nil
    end
  end

  def create
    amount = 30
    if params[:cards].present?
      card_id = params[:cards]
    else
      card_id = @customer.sources.create(source: params[:stripeToken]).id
    end

    payment = Payment.new(amount: amount, user: current_user)
    begin
      payment.process_payment(@customer_id, card_id)
      flash[:success] = 'Success - Payment Received'
      payment.save
    rescue Exception => e
      flash[:alert] = e.message
    end

    redirect_to checkout_index_path
  end

  private

  def get_customer
    if current_user.customer_id.nil?
      @customer = Stripe::Customer.create(
        email: current_user.email,
        source: params[:stripeToken]
      )
      @customer_id = @customer.id
    else
      @customer_id = current_user.customer_id
      @customer = Stripe::Customer.retrieve(@customer_id)
    end
  end
end