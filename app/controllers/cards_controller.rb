class CardsController < ApplicationController
  before_action :set_customer, only: [:edit, :update, :destroy]

  def index
    if current_user.customer_id.present?
      @cards = Stripe::Customer.retrieve(current_user.customer_id).sources.all(object: 'card')
    else
      @cards = nil
    end
  end

  def create
    customer = Stripe::Customer.retrieve(current_user.customer_id)
    begin
      customer.sources.create(source: params[:stripeToken])
      flash[:success] = 'Card successfully added to your account'
      redirect_to cards_path(current_user)
    rescue
      flash[:error] = 'Card could not be added to your account'
      render :new
    end
  end

  def new
    unless current_user.customer_id.present?
      customer = Stripe::Customer.create(
        email: current_user.email,
        source: params[:stripeToken]
      )
      current_user.customer_id = customer.id
      current_user.save
    end
  end

  def edit
    @card = Card.new
    @card_info = @customer.sources.retrieve(params[:id])
  end

  def update
    @card = Card.new(card_params)
    if @card.valid?
      card = @customer.sources.retrieve(params[:id])
      card.name = card_params[:card_name]
      card.exp_month = card_params[:card_expires_month]
      card.exp_year = card_params[:card_expires_year]
      begin
        card.save
        flash[:success] = 'Card details have been successfully updated'
        redirect_to cards_path
      rescue
        flash[:error] = 'Card details could not be updated'
        redirect_to edit_card_path(card.id)
      end
    end
  end

  def destroy
    respond_to do |format|
      begin
        @customer.sources.retrieve(params[:id]).delete
        format.json { redirect_to cards_path(current_user) }
      rescue
        format.json { render json: { success: true }, status: 400 }
      end

    end
  end

  private

  def set_customer
    @customer = Stripe::Customer.retrieve(current_user.customer_id)
  end

  def card_params
    params.require(:card).permit(
      :card_name, :card_expires_month, :card_expires_year
    )
  end
end