class Card
  include ActiveModel::Model

  attr_accessor :card_name, :card_number, :card_cvc, :card_expires_month, :card_expires_year

  validates :card_name, presence: true
  validates :card_expires_month, presence: true
  validates :card_expires_year, presence: true

  def self.month_options
    Date::MONTHNAMES.compact.each_with_index.map { |name, i| ["#{i+1} - #{name}", i + 1]}
  end

  def self.year_options
    (Date.today.year..(Date.today.year+20)).to_a
  end

end