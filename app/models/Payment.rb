class Payment < ApplicationRecord
  belongs_to :user

  def process_payment(customer_id, card_id = nil)
    charge = Stripe::Charge.create(
      amount: amount,
      currency: 'gbp',
      customer: customer_id,
      source: card_id,
      description: 'Some amazing product'
    )
    self.token = charge.id
  end
end